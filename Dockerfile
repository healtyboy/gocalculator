FROM alpine:latest
ADD ./calculator /
RUN chmod 755 calculator
CMD ["/calculator"]
