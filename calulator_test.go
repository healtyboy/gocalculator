package calculator

import "testing"

func TestSummary12(t *testing.T) {
	result := Summary(1, 2)
	expected := 3
	if result != expected {
		t.Errorf("It should return %d but got %d", expected, result)
	}
}

func TestSummary56(t *testing.T) {
	result := Summary(5, 6)
	expected := 11
	if result != expected {
		t.Errorf("It should return %d but got %d", expected, result)
	}
}
